function problem1(cashbox) {
    if (arguments.length !== 1 || cashbox.length === 0 || Array.isArray(cashbox)) {
        return [];
    }
    const newbox = [];
    cashbox.forEach(function (money) {
        const string = money.replace('$', '');
        const newString = string.split(',').join('');
        if (isNaN(newString)) {
            newbox.push(0);
        }
        else {
            newbox.push(Number.parseFloat(newString));
        }
    });
    return newbox;
}
