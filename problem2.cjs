function problem2(ipAddress) {
    const result = ipAddress.split('.');
    if (result.length !== 4) {
        return [];
    }
    result.forEach(function (number) {
        if (isNaN(number)) {
            return [];
        }
        else if (Number.parseInt(number) < 0 || Number.parseInt(number) > 255) {
            return [];
        }
    });
    return result;
}